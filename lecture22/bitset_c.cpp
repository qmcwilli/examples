#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    uint8_t s = 0;

    // Set 6th bit
    s |= 1<<6;
    cout << static_cast<int32_t>(s) << endl;

    // Test 6th bit
    if (s & 1<<6) {
    	cout << "Set!" << endl;
    }

    // Clear 6th bit
    s &= ~(1<<6);
    cout << static_cast<int32_t>(s) << endl;
    
    // Test 6th bit
    if (s & 1<<6) {
    	cout << "Set!" << endl;
    }

    return 0;
}
